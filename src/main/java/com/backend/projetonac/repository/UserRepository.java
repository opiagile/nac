package com.backend.projetonac.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.backend.projetonac.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String username);

	//User findbyEmail(String email);
}
