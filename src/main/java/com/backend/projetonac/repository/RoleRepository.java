package com.backend.projetonac.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.backend.projetonac.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
