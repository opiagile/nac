package com.backend.projetonac.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.backend.projetonac.entity.Role;
import com.backend.projetonac.entity.User;
import com.backend.projetonac.repository.RoleRepository;
import com.backend.projetonac.repository.UserRepository;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
		
		List<User> users = userRepository.findAll();
		if (users.isEmpty()) {
			this.createUsers("Francisco Araujo", "francisco@opiagile.com", passwordEncoder.encode("opi123"), "ROLE_ADMIN");
			this.createUsers("Felipe Araujo", "felipe@opiagile.com", passwordEncoder.encode("opi123"), "ROLE_CREDENCIADO");
			this.createUsers("Geraldo Araujo", "geraldo@opiagile.com", passwordEncoder.encode("opi123"), "ROLE_PARCEIRO");
			this.createUsers("Francisco Araujo", "faraujo82@gmail.com", passwordEncoder.encode("opi123"), "ROLE_ASSOCIADO");
		}
	}

	public void createUsers(String name, String email, String password, String role) {
		Role roleObject = new Role();
		roleObject.setName(role);
		
		this.roleRepository.save(roleObject);
		
		User user = new User(name, email, password, Arrays.asList(roleObject));
		userRepository.save(user);
	}
}
